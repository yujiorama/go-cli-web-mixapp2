package main

import (
	mixapp "bitbucket.org/yujiorama/go-cli-web-mixapp2"
	"log"
	"os"
)

func main() {

	if err := mixapp.WebMain(); err != nil {
		log.Fatalf("mixapp.WebMain error: %v\n", err)
	}

	defer os.Exit(0)
	return
}
