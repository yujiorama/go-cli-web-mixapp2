package main

import (
	mixapp "bitbucket.org/yujiorama/go-cli-web-mixapp2"
	"log"
	"os"
)

func main() {

	if err := mixapp.CliMain(); err != nil {
		log.Fatalf("mixapp.CliMain error: %v\n", err)
	}

	defer os.Exit(0)
	return
}
